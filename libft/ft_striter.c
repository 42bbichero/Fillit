/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/04 11:02:01 by bbichero          #+#    #+#             */
/*   Updated: 2018/09/26 22:00:28 by romontei         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void			ft_striter(char *s, void (*f)(char *))
{
	int		length;
	int		count;

	if (s != NULL && f != NULL)
	{
		length = ft_strlen(s);
		count = 0;
		while (count < length)
		{
			(*f)(s);
			s++;
			count++;
		}
	}
}
