/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/03 11:02:01 by bbichero          #+#    #+#             */
/*   Updated: 2015/04/14 08:49:21 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void			ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list		*list;
	t_list		*list_next;

	list = *alst;
	if (list)
	{
		while (list != NULL)
		{
			list_next = list->next;
			del(list->content, list->content_size);
			free(list);
			list = list_next;
		}
		*alst = NULL;
	}
}
