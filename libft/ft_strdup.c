/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/10 10:02:02 by bbichero          #+#    #+#             */
/*   Updated: 2015/04/14 08:54:13 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strdup(const char *src)
{
	char		*str;

	str = (char *)malloc(sizeof(char) * (ft_strlen(src) + 1));
	if (str != NULL)
	{
		ft_strcpy(str, src);
		return (str);
	}
	else
		return (0);
}
