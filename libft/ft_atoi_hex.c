/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_hex.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/07/26 12:29:54 by bbichero          #+#    #+#             */
/*   Updated: 2017/08/17 15:15:40 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void					ft_atoi_hex(void *ptr, int len, int new_line)
{
	char				*base;
	unsigned long long	address;

	base = "0123456789abcdef";
	address = (unsigned long long)ptr;
	ft_putstr("0x");
	if (new_line == 1)
		ft_putendl(ft_itohex(address, len));
	else
		ft_putstr(ft_itohex(address, len));
}
