/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itohex.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/16 11:06:48 by bbichero          #+#    #+#             */
/*   Updated: 2017/08/17 15:55:33 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_itohex(unsigned long long nbr, int len)
{
	char		*base;
	char		*hex;
	int			i;

	base = "0123456789abcdef";
	hex = (char *)malloc(sizeof(char) * len + 2);
	ft_bzero(hex, len + 2);
	i = len;
	while (nbr && i >= 0)
	{
		hex[i] = base[nbr % 16];
		nbr = nbr / 16;
		i--;
	}
	if (i > 0)
		hex = ft_strcpy(hex, &hex[i + 1]);
	return (hex);
}
