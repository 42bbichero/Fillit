/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_istrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 11:44:45 by bbichero          #+#    #+#             */
/*   Updated: 2014/11/07 18:18:46 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_istrchr(const char *s, int c)
{
	char		*str;
	int		i;

	str = (char *)s;
	i = 0;
	while (str && str[i] != c)
		i++;
	if (!str || str[i] != c)
		return (NULL);
	return (str + i);
}
