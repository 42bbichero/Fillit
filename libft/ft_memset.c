/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/24 10:19:59 by bbichero          #+#    #+#             */
/*   Updated: 2015/04/14 08:51:14 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void				*ft_memset(void *b, int c, size_t len)
{
	int				i;
	unsigned char	*string;

	string = (unsigned char *)b;
	i = 0;
	while (b && (int)len != i)
	{
		string[i] = (unsigned char)c;
		i++;
	}
	return (b);
}
