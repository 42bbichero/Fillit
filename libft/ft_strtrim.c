/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/03 11:02:01 by bbichero          #+#    #+#             */
/*   Updated: 2015/04/14 08:54:43 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char			*ft_strtrim(char const *s)
{
	int			i;
	int			j;
	int			length;
	char		*new_str;

	i = 0;
	j = 0;
	if (!s)
		return (NULL);
	length = ft_strlen((char *)s);
	if ((new_str = (char *)malloc((sizeof(char) * length) + 1)) == NULL)
		return (NULL);
	while (s[i] == ' ' || s[i] == '\n' || s[i] == '\t')
		i++;
	while (s[length - 1] == ' ' || s[length - 1] == '\n' || s[length - 1]
			== '\t')
		length--;
	while (i < length)
		new_str[j++] = s[i++];
	new_str[j] = '\0';
	return (new_str);
}
