/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_main.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/20 14:04:13 by bbichero          #+#    #+#             */
/*   Updated: 2018/10/16 23:03:56 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int				ft_init_pieces(t_pieces *pieces, int fd)
{
	char		*line;
	int			nb_line_tot;
	int			nb_line;
	t_pieces	*back_pc;

	nb_line = 1;
	nb_line_tot = 1;
	pieces->next = NULL;
	pieces->prev = NULL;
	pieces->tetri = (char **)malloc(sizeof(char *) * 5);
	back_pc = NULL;
	while (get_next_line(fd, &line) > 0)
	{
		if (nb_line_tot / 5 > 26 || ((nb_line % 5 == 0) && ft_strlen(line) != 0))
			return (-1);
		if (nb_line % 5 == 0)
		{
			pieces->tetri[nb_line - 1] = ft_strdup(line);
			pieces->tetri[4] = NULL;
			if (ft_near_piece(pieces->tetri) < 0)
				return (-1);
			back_pc = pieces;
			pieces->next = (t_pieces *)malloc(sizeof(t_pieces));
			pieces = pieces->next;
			pieces->tetri = (char **)malloc(sizeof(char *) * 5);
			pieces->prev = back_pc;
			pieces->next = NULL;
			nb_line = 0;
		}
		else
		{
			pieces->tetri[nb_line - 1] = ft_strdup(line);
			if (ft_strlen(line) != 4)
				return (-1);
		}
		printf("nb line : %d\n", nb_line);
		printf("nb line tot : %d\n", nb_line_tot);
		nb_line_tot++;
		nb_line++;
	}
	return (0);
}

int				ft_check_placement(char **tetri, int x, int y, t_plate *plate)
{
	t_plate		*back_p;
	int			yp;
	int			cpt;

	back_p = plate;
	yp = 0;
	cpt = 0;
	// remove unused vertical line
	while (tetri[x] && tetri[x][y])
	{
		x = 0;
		while (tetri[x] && tetri[x][y] && tetri[x][y] != 35)
			x++;
		if (tetri[x] && tetri[x][y] && tetri[x][y] == 35)
			break;
		y++;
	}

	// remove unused vertical line
	while (plate->line && plate->line[yp])
	{
		plate = back_p;
		while (plate->line && plate->line[yp] != '.')
			plate = plate->next;
		if (plate->line && plate->line[yp] == '.')
			break;
		yp++;
	}

	// check if there are available line for insert tetrimino
	if (!plate[x])
		plate = ft_enlarge(plate, ft_arr_struct(back_p));
	// check if plate contain 4 near point
	else if (!ft_check_points(plate))
		plate = ft_enlarge(plate, ft_arr_struct(back_p));

	plate = back_p;
	while (plate)
	{
		yp = 0;
		y = 0;
		while (plate->line && plate->line[yp])
		{
			if (cpt == 4)
				return (0);
			if (ft_isalpha(plate->line[yp]))
				return (-1);
			if (tetri[x] && tetri[x][y] && tetri[x][y] == 35)
			{
				cpt++;
				plate->line[yp] = tetri[x][y];
			}
			y++;
			yp++;
		}
		x++;
		plate = plate->next;
	}
	return (0);
}

void			ft_insert_tetri(t_plate *plate, char **tetri)
{
  int			x;
	int			y;

	x = 0;
	y = 0;
	// remove unused horizontal line
	while (!ft_strchr(plate->line, '.'))
	{
		printf("%s\n", "------>> Inside handle call");
		plate = ft_enlarge_struct(plate, ft_arr_struct(plate));

		printf("%s\n", "------>> Outside handle call");


		plate = plate->next;
	}

	// if plate is full of tetrimino, must enlarge
	if (!plate)
		ft_enlarge(plate, ft_arr_struct);

	// remove unused horizontal line
	while (!ft_strchr(tetri[x], 35))
		x++;

	if (ft_check_placement(tetri, x, 0, plate) < 0)
		plate = plate->next;
}

void			ft_resolve_tetris(t_pieces *pieces)
{
	t_plate		*plate;
	t_plate		*back_p;
	t_plate		*prev_p;
	int			cpt;

	plate = (t_plate *)malloc(sizeof(t_plate));
	back_p = plate;
	plate->prev = NULL;
	cpt = 0;
	while (cpt++ < 4)
	{
		plate->line = (char *)malloc(sizeof(char) * 5);
		plate->line = ft_strdup("....");
		plate->next = (t_plate *)malloc(sizeof(t_plate));
		prev_p = plate;
		plate = plate->next;
		plate->prev = prev_p;
		plate->next = NULL;
	}
	while (pieces)
	{
		ft_insert_tetri(back_p, pieces->tetri);
		pieces = pieces->next;
	}
}

/**
 * Function that start the program
 */
int				main(int ac, char **av)
{
	int			fd;
	t_pieces	*pieces;

	pieces = (t_pieces *)malloc(sizeof(t_pieces));
	if (ac != 2)
	{
		ft_putendl("Error, you must provide a filename\nUsage: ./fillit filename");
		return (-1);
	}
	fd = open(av[1], O_RDONLY);

	if (fd == -1)
	{
		ft_putendl("Error, invalid file or file doesn't exist.");
		return (-1);
	}
	else if (ft_init_pieces(pieces, fd) < 0)
	{
		ft_putendl("Error, can't parse given file.");
		return (-1);
	}
	ft_resolve_tetris(pieces);

	return (0);
}
