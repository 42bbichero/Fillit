/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_handle_plate.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: romontei <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/20 16:19:03 by romontei          #+#    #+#             */
/*   Updated: 2018/10/16 20:51:55 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

char		*ft_strrep(int size, char c)
{
	printf("%s\n", "Hello");
	char *s;
	int	i;

	i = 0;

	s = (char *)malloc(sizeof(char) * (size + 1));
	while (i <= size + 1)
	{
			s[i] = c;
			i++;
			printf("%d\n", i);
	}
	printf("The string is %s\n", s);
	return (s);
}


int			ft_arr_struct(t_plate *plate)
{
	t_plate *back_p;
	int		size;

	size = 0;
	back_p = plate;
	while (back_p)
	{
		size++;
		back_p = back_p->next;
	}
	return (size);
}

void		ft_print_struct(t_plate *plate)
{
	t_plate *back_p;

	back_p = plate;
	while (back_p)
	{
		ft_putendl(back_p->line);
		back_p = back_p->next;
	}
}

t_plate		*ft_enlarge_struct(t_plate *src, int size)
{
	t_plate	*dst;
	int		x;
	//int		y;

	dst = src;
	x = 0;
	while (x < size)
	{
		printf("x = %d\n", x);
		printf("size = %d\n", size);
		printf("line = %s\n", dst->line);
		if (x + 1 == size)
		{
			dst->next = (t_plate *)malloc(sizeof(t_plate));
			dst =  dst->next;
			dst->line = ft_strdup(ft_strrep(size, '.'));
			printf("%s\n", dst->line);
		}
		//y = 0;
		//while (dst->line && dst->line[y++]);
		printf("%s\n","Au bout de la ligne");
		//if (src->line)
		//{
		//	dst->line = (char *)malloc(sizeof(char) * (size + 1));
		//	dst->line = ft_strcat(ft_strdup(src->line), ".\0");
		//}
		x++;

		printf("%s\n","Fin de la premiere boucle" );
		dst = dst->next;
	}
	return (dst);
}

char		**ft_enlarge(char **src, int size)
{
	char	**dst;
	int		x;
	int		y;
	int		i;

	i = 0;
	dst = (char **)malloc(sizeof(char *) * (size + 1));
	while (src[i++])
		dst[i] = ft_strdup(src[i]);
	x = 0;
	while (x <= size)
	{
		if (x == size)
			dst[x] = ft_strdup(".....");
		y = 0;
		while (dst[x][y++]);
		if (src[x])
		{
			dst[x] = (char *)malloc(sizeof(char) * (size + 1));
			dst[x] = ft_strcat(ft_strdup(src[x]), ".\0");
		}
		x++;
	}
	dst[size + 1] = NULL;
	return (dst);
}
//res = ft_enlarge(test, arr_length(test));
