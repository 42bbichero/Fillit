/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <bbichero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 15:44:13 by bbichero          #+#    #+#             */
/*   Updated: 2018/10/17 11:54:04 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H
# include "libft/libft.h"
# include <fcntl.h>
# include <stdio.h>
# define BUF_SIZE 10

typedef struct		s_pieces
{
	char			**tetri;
	void			*next;
	void			*prev;
}					t_pieces;

typedef struct		s_plate
{
	void			*prev;
	char			*line;
	void			*next;
}					t_plate;

/*					get next line functions			*/
char		*ft_get_join(char *s1, char *s2);
int			get_next_line(int const fd, char **line);
int			ft_read(int fd, char **tmp);
void		ft_str(char **line, char **tmp, int *ret);

/*					fillit functions			*/
int			ft_init_parse(const int fd);
int			ft_init_pieces(t_pieces *pieces, int fd);
int			ft_check_tetri(char **line);
void		ft_resolve_tetris(t_pieces *pieces);
int			ft_near_piece(char **tetri);
int			ft_new_tetri(t_pieces *pieces, char *line, int nb_line, \
										t_pieces *back_pc);
int			ft_near_points(t_plate *plate, const int size, const int x);
int			ft_check_points(t_plate *plate);

t_plate		*ft_enlarge_struct(t_plate *plate, int size);
int			ft_arr_struct(t_plate *plate);
void		ft_print_struct(t_plate *plate);

#endif
