/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_check_tetri.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbichero <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/20 10:33:12 by bbichero          #+#    #+#             */
/*   Updated: 2018/10/16 21:44:33 by bbichero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int			ft_near_block(char **tetri, int x, int y)
{
	if (y == 0 && x > 0 && x < 3)
	{
		if (tetri[x - 1][y] != 35 && tetri[x][y + 1] != 35 && tetri[x + 1][y]
																		!= 35)
			return (-1);
	}
	else if (x == 0 && y > 0 && y < 3)
	{
		if (tetri[x][y - 1] != 35 && tetri[x + 1][y] != 35 && tetri[x][y + 1]
																		!= 35)
			return (-1);
	}
	else if (x == 3 && y > 0 && y < 3)
	{
		if (tetri[x][y - 1] != 35 && tetri[x][y + 1] != 35)
			return (-1);
	}
	else if (y == 3 && x > 0 && x < 3)
	{
		if (tetri[x - 1][y] != 35 && tetri[x][y - 1] != 35 && tetri[x + 1][y]
																		!= 35)
			return (-1);
	}
	return (0);
}

// check in plate if there is any place for a tetrimino
int			ft_near_point(t_plate *plate, const int size, const int x)
{
	if (x == 0 && plate->next && plate->prev)
	{
		if (plate->prev->line[x] != 46 && plate->line[x + 1] != 46
													&& plate->next[x] != 46)
			return (-1);
	}
	else if (plate->prev && x > 0 && x < size)
	{
		if (plate->line[x - 1] != 46 && plate->next->line[x] != 46
											&& plate->line[x + 1] != 46)
			return (-1);
	}
	else if (!plate->next && x > 0 && x < size)
	{
		if (plate->line[x - 1] != 46 && plate->line[x + 1] != 46)
			return (-1);
	}
	else if (x == size && plate->next && plate->prev)
	{
		if (plate->prev[x] != 46 && plate->line[x - 1] != 46
													&& plate->next[x] != 46)
			return (-1);
	}
	return (0);
}

int			ft_near_piece(char **tetri)
{
	int		cpt;
	int		x;
	int		y;

	cpt = 0;
	x = 0;
	while (tetri[x])
	{
		y = 0;
		printf("\ntetri[%d] = %s\n", x, tetri[x]);
		while (tetri[x][y])
		{
			printf("tetri[%d][%d] = %c ", x, y, tetri[x][y]);
			if (tetri[x][y] == 35)
			{
				cpt++;
				if (ft_near_block(tetri, x, y) < 0)
					return (-1);
			}
			else if (tetri[x][y] != 46)
				return (-1);
			y++;
		}
		x++;
	}
	ft_putendl("---");
	return (cpt != 4 ? -1 : 0);
}
